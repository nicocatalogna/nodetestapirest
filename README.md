# SPA Single Page Application Almundo.com

Ejercicio de Prueba de Selección. Esta Api fue pensada para la web alojada en https://bitbucket.org/dejotajcq/almundo_angular4.git

### Instalación

Requiere [Node.js](https://nodejs.org/) para correr.
Servidor SQL Server local o en su defecto usar el alojado en amazon 'nodeapirest.czasetlkclhe.sa-east-1.rds.amazonaws.com,1433'
El servidor esta abierto a internet para las pruebas.
En caso de requerirlo modificar en archivo 'nodetestapirest\src\enviroment' el config 'enviroment.dbConfig' los datos de conexion.

Instalar las dependencias e iniciar el servidor.

```sh
$ git clone https://nicocatalogna@bitbucket.org/nicocatalogna/nodetestapirest.git
$ cd nodetestapirest
$ npm install o yarn install
$ npm build o yarn build (Consola 1)
$ npm start o yarn start (Consola 2)
```
![](https://bytebucket.org/nicocatalogna/nodetestapirest/raw/94999a8f58c676cb29ba9f9d8e1ad175009096a5/gif/InicializarEjecutarConsulta.gif?token=a6bf62c5b41512b1dd3fb4104bc5d46e0f3677e7)

Para consumir la Api y testearla pueden utilizar el archivo exportado de Postman en el archivo 'nodetestapirest\postman\NodeApiRestTest.postman_collection.json'.
![](https://bytebucket.org/nicocatalogna/nodetestapirest/raw/94999a8f58c676cb29ba9f9d8e1ad175009096a5/gif/Postman.gif?token=ba5656911b1d4ae92d8468d22279dbea3d109468)

### La Api contiene
DataController (Para cargar el Data.Json)
HotelesController (Para Buscar, Editar o Eliminar hoteles)
amenitiesController (Para buscar amenities)

### Arquitectura
Esta construido en lenguaje Typescript.
Los complementos utilizados son  Express (Para la API rest), mssql (para SQL Server) y typeORM para el ORM de la base de datos.

