const { join } = require('path')
var webpack = require('webpack');  
var fs = require('fs');

var nodeModules = {};
fs.readdirSync('node_modules')
    .filter(function(x) {
        return ['.bin'].indexOf(x) === -1;
    })
    .forEach(function(mod) {
        nodeModules[mod] = 'commonjs ' + mod;
    });

//console.log('Node Modules: '+ JSON.stringify(nodeModules));
module.exports = {

  // Pick any source-map that does not require eval.
  // `inline-source-map` gives the best quality for development.
  devtool: 'inline-source-map',
  externals: nodeModules,
  entry: join(__dirname, 'src/server'),

  output: {
    path: join(__dirname, 'build'),
    filename: 'server.js',

    // Bundle absolute resource paths in the source-map,
    // so VSCode can match the source file.
    devtoolModuleFilenameTemplate: '[absolute-resource-path]'
  },
  
  watch: true,

  resolve: {
    extensions: ['.ts']
  },

  // Add minification
/*   plugins: [
    new webpack.optimize.UglifyJsPlugin()
  ], 
   modulesDirectories: ["node_modules"],
  */    
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin()
  ], 
  module: {
      loaders: [
      {
        test: /\.ts$/,   
        loaders: ['ts-loader']
      }
    ]
  }

  ,node: {
    console: false,
    fs: 'empty',
    net: 'empty',
    tls: 'empty'
  }
}