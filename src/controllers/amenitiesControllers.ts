import {Request, Response} from "express";
import {getManager, createConnection, getConnection, ConnectionManager, getConnectionManager, Connection, getRepository, getCustomRepository} from "typeorm";

import { usuarios } from './../entity/usuarios';
import { amenitiesRepository } from "./../Repository/amenitiesRepository";
import { amenitie } from './../entity/amenities';
import { hotele } from './../entity/hoteles';
import { Environment } from "../environment";
import { hotelesRepository } from '../Repository/hotelesRepository';


export async function GetAmenities(request: Request, response: Response) {
    let result: amenitie[] = [];
    try {
        const page = request.query.page === null ? 0 : request.query.page; 
        const per_page = request.query.per_page === null ? 0 : request.query.per_page; 
        ///hoteles?page=1&per_page=10
        const amenitiesRep = getCustomRepository(amenitiesRepository);
        
        result = await amenitiesRep.findForPage(page, per_page);

        console.log("finish");

        response.send(result);

    } catch (error) {
        // TODO: Devolver mensaje lindo y limpiar ante caso de injeccion.
        console.log(error);
    }
}