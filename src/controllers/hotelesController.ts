import {Request, Response} from "express";
import {getManager, createConnection, getConnection, ConnectionManager, getConnectionManager, Connection, getRepository, getCustomRepository} from "typeorm";

import { usuarios } from './../entity/usuarios';
import { amenitiesRepository } from "./../Repository/amenitiesRepository";
import { amenitie } from './../entity/amenities';
import { hotele } from './../entity/hoteles';
import { Environment } from "../environment";
import { hotelesRepository } from '../Repository/hotelesRepository';
import { ResponseModel } from "../models/ResponseModel";
import { hotelModel } from "../models/hotelModel";


export async function GetHoteles(request: Request, response: Response) {    
    let result: ResponseModel = new ResponseModel();
    try {
        ///hoteles?page=1&per_page=10
        const page = request.query.page === null ? 1 : request.query.page; 
        const per_page = request.query.per_page === null ? 10 : request.query.per_page; 

        const hotelesRep = getCustomRepository(hotelesRepository);

        //no tengo nada
        const starts = request.query.starts; 
        const name = request.query.name;

        if (starts !== undefined) {
            result.data = await hotelesRep.findStartForPage(starts, page, per_page);
        } else if (name !== undefined) {
            result.data = await hotelesRep.findNameForPage(name, page, per_page);
        }
        else {
            result.data = await hotelesRep.findForPage(page, per_page);
        }
        
        result.mensage = 'Exitoso';
        
        console.log("finish");

        response.send(result);

    } catch (error) {
        // TODO: Devolver mensaje lindo y limpiar ante caso de injeccion.
        console.log(error);
        response.sendStatus(500);
    }
}

export async function GetHotel(request: Request, response: Response) {
    let result: ResponseModel = new ResponseModel();
    try {
        //Leo Archivo
        const id = request.params.id;

        if (!id) return response.sendStatus(400); //Si no hay id chau

        const hotelesRep = getCustomRepository(hotelesRepository);

        result.data = await hotelesRep.findOneById(id);

        console.log("finish");

        response.send(result);

    } catch (error) {
        // TODO: Devolver mensaje lindo y limpiar ante caso de injeccion.
        console.log(error);
    }
}

export async function DeleteHotel(request: Request, response: Response) {
    let result: ResponseModel = new ResponseModel();
    try {
        //Leo Archivo
        const id = request.params.id;

        if (!id) return response.sendStatus(400); //Si no hay id chau

        const hotelesRep = getCustomRepository(hotelesRepository);

        result.data = await hotelesRep.findByIdAndAmenities(id);

        if (result.data != null) {
            await hotelesRep.findAndDelete(result.data[0]);
            result.mensage = 'Se elimino Correctamente';
            result.estado = true;
        } 
        else{
            result.mensage = 'No existe id: '+ id;
            result.estado = false;
        } 

        console.log("finish");

        response.send(result);

    } catch (error) {
        // TODO: Devolver mensaje lindo y limpiar ante caso de injeccion.
        console.log(error);
    }
}

// TODO: Terminar
export async function PostHotel(request: Request, response: Response) {
    let result: ResponseModel = new ResponseModel();
    try {
        //Leo Archivo
        if (!request.body) return response.sendStatus(400);
        
        let hotelresult: hotelModel = <hotelModel>request.body;

        const hotelesRep = getCustomRepository(hotelesRepository);

        result.data = await hotelesRep.findOneById(hotelresult.id);

        if (result.data == null) {
            await hotelesRep.save(hotelresult);
            result.mensage = 'Se creo Correctamente';
            result.estado = true;
        } 
        else{
            result.mensage = 'No existe id: '+ hotelresult.id;
            result.estado = false;
        } 

        console.log("finish");

        response.send(result);

    } catch (error) {
        // TODO: Devolver mensaje lindo y limpiar ante caso de injeccion.
        console.log(error);
    }
}
