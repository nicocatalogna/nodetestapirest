import {Request, Response} from "express";
import {getManager, createConnection} from "typeorm";
import {usuarios} from "../entity/usuarios";
import { Environment } from "../environment";

/**
 * Loads all posts from the database.
 */
    export async function GetAllAction(request: Request, response: Response) {
        try {
            let enviroment: Environment = new Environment();

            createConnection({
                type: "mssql",
                host: enviroment.dbConfig.server,
                port: enviroment.dbConfig.port,
                username: enviroment.dbConfig.user,
                password: enviroment.dbConfig.password,
                database: enviroment.dbConfig.database,
                entities: [usuarios]            
            }).then(async connection => {
            
                // get a usuarios repository to perform operations with usuarios
                const usuariosRepository = connection.getRepository(usuarios);
            
                // load a post by a given post id
                const user = await usuariosRepository.findOneById(1);

                connection.close();

                // return loaded posts
                response.send(user);
    /* 
                let post = new Post();
                post.text = "Hello how are you?";
                post.title = "hello";
                post.likesCount = 100;
            
                let postRepository = connection.getRepository(Post);
            
                postRepository
                    .persist(post)
                    .then(post => console.log("Post has been saved: ", post))
                    .catch(error => console.log("Cannot save. Error: ", error)); */
            
            }, error => console.log("Cannot connect: ", error));

        
        } catch (error) {
            console.log(error);
        }
    }

    export async function GetById(request: Request, response: Response) {
        try {            
            let enviroment: Environment = new Environment();
            
            const id = request.params.id;

            createConnection({
                type: "mssql",
                host: enviroment.dbConfig.server,
                port: enviroment.dbConfig.port,
                username: enviroment.dbConfig.user,
                password: enviroment.dbConfig.password,
                database: enviroment.dbConfig.database,
                entities: [usuarios]            
            }).then(async connection => {
            
                 // get a usuarios repository to perform operations with usuarios
                const usuariosRepository = connection.getRepository(usuarios);
            
                // load a post by a given post id
                const user = await usuariosRepository.findOneById(id);
    
                
                connection.close();
    
                // return loaded posts
                response.send(user);
    /* 
                let post = new Post();
                post.text = "Hello how are you?";
                post.title = "hello";
                post.likesCount = 100;
            
                let postRepository = connection.getRepository(Post);
            
                postRepository
                    .persist(post)
                    .then(post => console.log("Post has been saved: ", post))
                    .catch(error => console.log("Cannot save. Error: ", error)); */
            
            }, 
                error => 
                    console.log("Cannot connect: ", error));
    
           
        } catch (error) {
            console.log(error);
        }
}