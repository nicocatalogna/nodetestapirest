import { usuarios } from './../entity/usuarios';
import { amenitiesRepository } from "./../Repository/amenitiesRepository";
import { amenitie } from './../entity/amenities';
import { hotele } from './../entity/hoteles';

import {Request, Response} from "express";
import {getManager, createConnection, getConnection, ConnectionManager, getConnectionManager, Connection, getRepository, getCustomRepository} from "typeorm";
import { Environment } from "../environment";
import { dataModel } from '../models/dataModel';
(Symbol as any).asyncIterator = Symbol("Symbol.asyncIterator");

export async function DataAction(request: Request, response: Response) {
    try {
        //Leo Archivo data.json
        let result: hotele[] = [];
        if (!request.body) return response.sendStatus(400);

        let dataresult: dataModel[] = <dataModel[]>request.body;

        for await (const item of dataresult){
            let hotel = await SalvarData(item);
            result.push(hotel);
        }
  
        console.log("finish");

        response.send(result);

    } catch (error) {
        // TODO: Devolver mensaje lindo y limpiar ante caso de injeccion.
        console.log(error);
        response.sendStatus(500);
    }
}

async function SalvarData(dataSend: dataModel): Promise<hotele> {

    let enviroment: Environment = new Environment();
    try {       
        //Nuevo hotel
        let hotel: hotele = new hotele();
        
        hotel.id = dataSend.id;
        hotel.image = dataSend.image;
        hotel.name = dataSend.name;
        hotel.price = dataSend.price;
        hotel.stars = dataSend.stars;
        hotel.amenities = [];
                
        let resAmanitie = await amenitiesSerchs(dataSend.amenities);

        hotel.amenities = resAmanitie;
        
        // get a usuarios repository to perform operations with usuarios
        const hotelesRepository = getManager().getRepository(hotele);
        
        // load a post by a given post id
        hotel = await hotelesRepository.save(hotel);
        
        console.log('Save Hotel ' + hotel.id);
        return hotel;
    } catch (error) {
        console.log(error);
        throw error;
    }  
}

async function amenitiesSerchs(amenitiesItems: string[]): Promise<amenitie[]> {
    
        //Buscar amenities
        const amenitiesRepo = getCustomRepository(amenitiesRepository);

        let result: amenitie[] = [];

        for await (const item of amenitiesItems) {
            let stringName = item.toLowerCase();
            
            console.log(stringName);

            let amenitiesResult = await amenitiesRepo.findByName(stringName);
            
            if (amenitiesResult == null) {
                //Nuevo amenities
                let newAmenities = new amenitie();
                newAmenities.name = stringName; 
        
                if (stringName == 'safety-box') {
                    console.log('Entre ' + stringName);
                }
        
                amenitiesResult = await amenitiesRepo.save(newAmenities);                
            } 

            result.push(amenitiesResult);
        }

        return result;   
    }
    

async function amenitiesSerch(amenitiesItems: string): Promise<amenitie> {

    //Buscar amenities
    const amenitiesRepo = getCustomRepository(amenitiesRepository);
    let stringName = amenitiesItems.toLowerCase();
    
    let amenitiesResult = await amenitiesRepo.findByName(stringName);

    if (amenitiesResult == null) {
        //Nuevo amenities
        let newAmenities = new amenitie();
        newAmenities.name = stringName; 

        if (stringName == 'safety-box') {
            console.log('Entre ' + stringName);
        }

        amenitiesResult = await amenitiesRepo.save(newAmenities);
    } 

    return amenitiesResult;   
}