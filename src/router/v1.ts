import * as express from 'express';
import {GetAllAction, GetById} from '../controllers/usuariosController';
import { DataAction } from '../controllers/dataController';
import { GetHoteles, GetHotel, DeleteHotel } from '../controllers/hotelesController';
import { GetAmenities } from '../controllers/amenitiesControllers';

export default(app) => {

    const apiRoutes = express.Router();
    
    
    /**
     *  ROUTES
     */
    const hotelesRouster = express.Router();
    apiRoutes.use('/hoteles', hotelesRouster);
    
        hotelesRouster.get('/', GetHoteles);
        hotelesRouster.get('/hotel/:id', GetHotel);    
        hotelesRouster.delete('/hotel/:id', DeleteHotel);    
        
    const amenitiesRouster = express.Router();
    apiRoutes.use('/amenities', amenitiesRouster);
    
        amenitiesRouster.get('/', GetAmenities);
        //postRouster.get('/hotel/:id', GetAmenitie);    
        
    const usuariosRouster = express.Router();     
    apiRoutes.use('/usuarios', usuariosRouster);

        usuariosRouster.get('/', GetAllAction);
        usuariosRouster.get('/:id', GetById);    

    const dataRouster = express.Router();
    apiRoutes.use('/data', dataRouster);
    
        dataRouster.post('/', DataAction);  
    

    app.use('/api', apiRoutes);


}

