export class dataModel {
    public id: number;
    public name: string;
    public stars: number;
    public price: number;
    public image: string;
    public amenities: string[];
}