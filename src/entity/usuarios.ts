import 'reflect-metadata';
import {Entity, PrimaryGeneratedColumn, Column, BaseEntity} from "typeorm";

@Entity()
export class usuarios extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nombre: string;

    @Column()
    apellido: string;

    @Column()
    usuario: string;

    @Column()
    contraseña: string;

    @Column()
    fechaRegistro: Date;

    @Column()
    correo: string;

}