import {Entity, PrimaryGeneratedColumn, Column, ManyToMany} from "typeorm";
import { hotele } from "./hoteles";


@Entity()
export class amenitie {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    description: string;

    @ManyToMany(type => hotele, hotele => hotele.id)
    hoteles: hotele[];
}