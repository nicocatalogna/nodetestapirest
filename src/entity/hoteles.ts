import {Entity, PrimaryGeneratedColumn, Column, ManyToMany, JoinTable} from "typeorm";
import { amenitie } from "./amenities";


@Entity()
export class hotele {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    stars: number;

    @Column("money")
    price: number;

    @Column()
    image: string;
   
    @ManyToMany(type => amenitie, amenitie => amenitie.id)
    @JoinTable()
    amenities: amenitie[];

}