import { amenitie } from './../entity/amenities';
import {EntityRepository, Repository} from "typeorm";
import {hotele} from "../entity/hoteles";

@EntityRepository(hotele)
export class hotelesRepository extends Repository<hotele> {

    findByName(name: string) {
        return this.findOne({ name });
    }

    findByIdAndAmenities(id: string) {
        return this.createQueryBuilder("hotele")
            .leftJoinAndSelect("hotele.amenities", "amenitie")
                .where("hotele.Id = :id")
                .setParameters({ id: id })
                .getMany();
    }

    findAndDelete(hotel: hotele){
        let arrayAmenities = [];

        for (var key in hotel.amenities) {
            arrayAmenities.push({id: hotel.amenities[key]});
        }

        this.createQueryBuilder("hotele")
            .relation(hotele, "amenitie")
            .of({ id: hotel.id }) // post
            .remove(arrayAmenities);

        this.remove(hotel);
    }
    findForPage(page: number, per_page: number) {
      return this.createQueryBuilder("hotele")
        .leftJoinAndSelect("hotele.amenities", "amenitie")
            .orderBy("hotele.Id")
            .offset(page)
            .limit(per_page)
            .getMany();
    }

    findStartForPage(stars: number, page: number, per_page: number) {
        return this.createQueryBuilder("hotele")
          .leftJoinAndSelect("hotele.amenities", "amenitie")
              .where("hotele.stars = :stars")
              .orderBy("hotele.Id")
              .offset(page)
              .limit(per_page)
              .setParameters({ stars: stars })
              .getMany();
    }

    findNameForPage(name: number, page: number, per_page: number) {
        return this.createQueryBuilder("hotele")
          .leftJoinAndSelect("hotele.amenities", "amenitie")
              .where("hotele.name like :name")
              .orderBy("hotele.Id")
              .offset(page)
              .limit(per_page)
              .setParameters({ name: '%' + name + '%' })
              .getMany();
    }

}
