import { amenitie } from './../entity/amenities';

import {EntityRepository, Repository} from "typeorm";

@EntityRepository(amenitie)
export class amenitiesRepository extends Repository<amenitie> {

    findByName(name: string) {      
        return  this.findOne({ name });
    }

    CreateForName(name: string) {
        let amanitie = this.create();
        amanitie.name = name;
        return this.save(amanitie);         
    }

    
    findForPage(page: number, per_page: number) {
        return this.createQueryBuilder("amenitie")
              .orderBy("amenitie.Id")
              .offset(page)
              .limit(per_page)
              .getMany();
      }

}
