import * as express from 'express'
import * as bodyParser from 'body-parser'
import * as cookieParser from 'cookie-parser'
import * as helmet from 'helmet'
import * as cors from 'cors'
import * as morgan from 'morgan'
import * as http from 'http';

import { Environment } from './environment';
import  router  from './router/v1';
import { ConnectionManager, getConnectionManager, Connection } from 'typeorm';

import { usuarios } from './entity/usuarios';
import { amenitie } from './entity/amenities';
import { hotele } from './entity/hoteles';

export class App {
    public express: express.Application;
    public enviroment: Environment = new Environment();
    public httpServer: http.Server;

    constructor() {
        // inicializa servidor
        this.express = express();

        // express middlewares
        this.express.use(bodyParser.urlencoded({extended:false}));
        this.express.use(bodyParser.json());
        //this.express.use(bodyParser.json({ type: 'application/*+json' }))
        this.express.use(cookieParser());
        this.express.use(morgan('dev'));
        this.express.use(helmet());
        this.express.use(cors());

        // router
        router(this.express);
        
        // DB
        this.connectionDb();

        this.httpServer = this.express.listen(this.enviroment.port, ()=> {
            console.log(`Server listening on port ${this.enviroment.port}`)
        });

        console.log("Server Init");
    }

    async connectionDb(): Promise<void> {
        const connectionManager: ConnectionManager = getConnectionManager(); 
        
        const connection: Connection = connectionManager.create({
            type: "mssql",
            host: this.enviroment.dbConfig.server,
            port: this.enviroment.dbConfig.port,
            username: this.enviroment.dbConfig.user,
            password: this.enviroment.dbConfig.password,
            database: this.enviroment.dbConfig.database,
            logging: true,
            entities: [usuarios, hotele, amenitie]            
        });
        
        await connection.connect(); // performs connection
    }
}

export default new App().express;

