CREATE TABLE [dbo].[usuarios] (
    [Id]            INT           NOT NULL,
    [nombre]        VARCHAR (255) NOT NULL,
    [apellido]      VARCHAR (255) NOT NULL,
    [usuario]       VARCHAR (100) NOT NULL,
    [contraseña]    VARCHAR (255) NOT NULL,
    [fechaRegistro] DATE          NOT NULL,
    [correo]        VARCHAR (255) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

CREATE TABLE [dbo].[hotele] (
    [Id]            INT           NOT NULL,
    [name]          VARCHAR (255) NOT NULL,
    [stars]         int,
    [price]         money,
    [image]         VARCHAR (255)
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

CREATE TABLE [dbo].[amenitie] (
    [Id]          INT  IDENTITY (1,1) NOT NULL,
    [name]        VARCHAR (255) NOT NULL,
    [description]      VARCHAR (255)
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

CREATE TABLE [dbo].[hotele_amenities_amenitie] (
	[Id] INT IDENTITY(1,1) NOT NULL,
    [amenitieId] INT NOT NULL,
    [hoteleId] INT NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY (amenitieId) references amenitie(Id),
    FOREIGN KEY (hoteleId) references hotele(Id)
);
